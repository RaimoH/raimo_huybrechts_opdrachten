USE raimohuybrechts;
CREATE TABLE UnitBase
(
    Id INT NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(255) CHARACTER SET utf8 NOT NULL UNIQUE,
    Description VARCHAR(1024) CHARACTER SET utf8,
    ShippingCostMultiplier FLOAT,
    Code VARCHAR(2) CHARACTER SET utf8 NOT NULL UNIQUE
);