-- Select row based on Id
USE raimohuybrechts;
DROP PROCEDURE IF EXISTS UnitBaseSelectOne;
DELIMITER //
CREATE PROCEDURE UnitBaseSelectOne(
	pId int
)
BEGIN
SELECT `UnitBase`.`Id`,
	`UnitBase`.`Name`,
	`UnitBase`.`Description`,
	`UnitBase`.`ShippingCostMultiplier`,
	`UnitBase`.`Code,`
	FROM `UnitBase`
	WHERE pId = `UnitBase`.`Id`
	ORDER BY `Name`;
END //
DELIMITER ;


-- Select all rows
USE raimohuybrechts;
DROP PROCEDURE IF EXISTS UnitBaseSelectAll;
DELIMITER //
CREATE PROCEDURE UnitBaseSelectAll()
BEGIN
SELECT `UnitBase`.`Id`,
	`UnitBase`.`Name`,
	`UnitBase`.`Description`,
	`UnitBase`.`ShippingCostMultiplier`,
	`UnitBase`.`Code`
	FROM `UnitBase`
	ORDER BY `Name`;
END //
DELIMITER ;


-- Insert row
USE raimohuybrechts;
DROP PROCEDURE IF EXISTS UnitBaseInsert;
DELIMITER //
CREATE PROCEDURE `UnitBaseInsert`
(
	IN pName VARCHAR(255) CHARACTER SET utf8,
	IN pDescription VARCHAR(1024) CHARACTER SET utf8,
    IN pShippingCostMultiplier FLOAT,
    IN pCode VARCHAR(2),
	OUT pId INT 
)
BEGIN
INSERT INTO `UnitBase`
	(
		`UnitBase`.`Name`,
		`UnitBase`.`Description`,
		`UnitBase`.`ShippingCostMultiplier`,
        `UnitBase`.`Code`        
	)
	VALUES
	(
		pName,
		pDescription,
        pShippingCostMultiplier,
        pCode
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;


-- Delete row
USE raimohuybrechts;
DROP PROCEDURE IF EXISTS UnitBaseDelete;
DELIMITER //
CREATE PROCEDURE UnitBaseDelete (
    IN pId INT
    )
BEGIN
	DELETE FROM `UnitBase` 
    WHERE `UnitBase`.`Id` = pId;
END //
DELIMITER ;


-- Update row
USE raimohuybrechts;
DROP PROCEDURE IF EXISTS UnitBaseUpdate;
DELIMITER //
CREATE PROCEDURE `UnitBaseUpdate`
(
	IN pName VARCHAR(255) CHARACTER SET utf8,
	IN pDescription VARCHAR(1024) CHARACTER SET utf8,
    IN pShippingCostMultiplier FLOAT,
    IN pCode VARCHAR(2) CHARACTER SET utf8,
	IN pId INT 
)
BEGIN
UPDATE `UnitBase`
	SET `UnitBase`.`Name` = pName,
		`UnitBase`.`Description` = pDescription,
		`UnitBase`.`ShippingCostMultiplier` = pShippingCostMultiplier,
		`UnitBase`.`Code` = pCode
WHERE `UnitBase`.`Id` = pId;
END //
DELIMITER ;