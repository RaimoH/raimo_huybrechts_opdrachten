<?php
namespace Programmeren4\UnitBase\Model;

class UnitBase extends\ModernWays\Mvc\Model
{
    private $id;
    private $name;
    private $description;
    private $shippingCostMultiplier;
    private $code;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function GetName()
    {
        return $this->Name;
    }
    
    public function getNameUpperCase()
    {
        return strtoupper($this->name);
    }
    
    public function setName($name)
    {
         $this->name = $name;  
    }
    
    public function getDescription()
    {
        return $this->Description;
    }
    
    public function setDescription($Description)
    {
        $this->description = $description;
    }
    
    public function getCode()
    {
        return $this->code;
    }
    
    public function setCode($code)
    {
        $this->code = $code;
    }
    
    public function getShippingCostMultiplier()
    {
        return $this->shippingCostMultiplier;
    }
    
    public function setShippingCostMultiplier($ShippingCostMultiplier)
    {
        $this->shippingcostmultiplier = $ShippingCostMultiplier;
    }
}