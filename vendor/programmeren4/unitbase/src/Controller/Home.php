<?php
namespace Programmeren4\UnitBase\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    private $pdo;
    private $model;
    
    public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\INoticeBoard $noticeBoard = null)
    {
        parent::__construct($route, $noticeBoard);
        
        $modelState = new \ModernWays\Dialog\Model\NoticeBoard();
        $this->model = new \Programmeren4\UnitBase\Model\UnitBase($modelState);
        
        $this->noticeBoard->startTimeInKey('PDO Connection');

        try {
            $this->pdo = new \PDO('mysql:host=localhost;dbname=raimohuybrechts;charset=utf8', 'digifais', '');
            
            $this->noticeBoard->setText("PDO connectie gelukt.");
            $this->noticeBoard->setCaption('PDO connectie voor UnitBase.');
        }
        
        catch (\Exception $e) {
            $this->noticeBoard->setText("{$e->getMessage()} op lijn {$e->getLine()} : {$e->getFile()}.");
            $this->noticeBoard->setCaption('PDO connectie voor UnitBase.');
            $this->noticeBoard->setCode($e->getCode());
        }
        
        $this->noticeBoard->log();
    }
    
    public function editing()
    {
        if ($this->pdo) {
            $statement = $this->pdo->query("CALL UnitBaseSelectAll");
            
            $this->model->setList($statement->fetchAll(\PDO::FETCH_ASSOC));
            
            return $this->view('Home','Editing', $this->model);
        }
        
        else {
            $this->view('Home', 'Error', null);
        }
    }
    
    public function inserting()
    {
        return $this->view('Home', 'Inserting', null);
    }
    
    public function insert()
    {
        $this->model->setName(filter_input(INPUT_POST, 'UnitBaseName', FILTER_SANITIZE_STRING));
        $this->model->setDescription(filter_input(INPUT_POST, 'UnitBaseDescription', FILTER_SANITIZE_STRING));
        $this->model->setShippingCostMultiplier(filter_input(INPUT_POST, 'UnitBaseShippingCostMultiplier', FILTER_SANITIZE_NUMBER_FLOAT));
        $this->model->setCode(filter_input(INPUT_POST, 'UnitBaseCode', FILTER_SANITIZE_STRING));
        
        if (!$this->pdo) {
            return $this->view('Home', 'Error', null);
        }
        
        elseif ($this->model->isValid()) {
            $statement = $this->pdo->prepare("CALL UnitBaseInsert(:pName, :pDescription, :pShippingCostMultiplier, :pCode, @pId)");

            $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $statement->bindValue(':pDescription', $this->model->getDescription(), \PDO::PARAM_STR);
            $statement->bindValue(':pShippingCostMultiplier', $this->model->getShippingCostMultiplier(), \PDO::PARAM_STR);
            $statement->bindValue(':pCode', $this->model->getCode(), \PDO::PARAM_STR);
            
            $statement->execute();
            
            $this->model->setId($this->pdo->query('SELECT @pId')->fetchColumn());
            
            header("Location: /unitbase.php/Home/Editing");
        }
        
        else {
            return $this->view('Home', 'Inserting', $this->model);
        }
    }
    
    public function updating()
    {
        if ($this->pdo) 
        {
            $this->model->setId($this->route->getId());
            
            $statement = $this->pdo->prepare("CALL UnitBaseSelectOne(:pId)");
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $statement->execute();
            
            $array = $statement->fetch(\PDO::FETCH_ASSOC);
            
            $this->model->setName($array['Name']);
            $this->model->setDescription($array['Description']);
            $this->model->setShippingCostMultiplier($array['ShippingCostMultiplier']);
            $this->model->setCode($array['Code']);
            
            return $this->view('Home', 'Updating', $this->model);
        }
        
        else 
        {
            return $this->view('Home', 'Error', null);
        }
        
    }
    
    public function update()
    {
        if($this->pdo) {
            $this->model->setId(filter_input(INPUT_POST, 'UnitBaseId', FILTER_SANITIZE_NUMBER_INT));
            $this->model->setName(filter_input(INPUT_POST, 'UnitBaseName', FILTER_SANITIZE_STRING));
            $this->model->setDescription(filter_input(INPUT_POST, 'UnitBaseDescription', FILTER_SANITIZE_STRING));
            $this->model->setShippingCostMultiplier(filter_input(INPUT_POST, 'UnitBaseShippingCostMultiplier', FILTER_SANITIZE_NUMBER_FLOAT));
            $this->model->setCode(filter_input(INPUT_POST, 'UnitBaseCode', FILTER_SANITIZE_STRING));
        
            if($this->model->isValid()) {
                $statement = $this->pdo->prepare("CALL UnitBaseUpdate(:pName, :pDescription, :pCode, :pShippingCostMultiplier, :pId)");
                
                $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
                $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
                $statement->bindValue(':pDescription', $this->model->getDescription(), \PDO::PARAM_STR);
                $statement->bindValue(':pShippingCostMultiplier', $this->model->getShippingCostMultiplier(), \PDO::PARAM_STR);
                $statement->bindValue(':pCode', $this->model->getCode(), \PDO::PARAM_STR);
                
                $statement->execute();
                
                header("Location: /unitbase.php/Home/Editing");
            }
            
            else {
                return $this->view('Home', 'Updating', $this->model);
            }
        }
        
        else {
            return $this->view('Home', 'Error', null);
        }
        
    }
    
    public function delete()
    {
        if($this->pdo) {
            $statement = $this->pdo->prepare("CALL UnitBaseDelete(:pId)");
            
            $statement->bindValue(':pId', $this->route->getId(), \PDO::PARAM_INT);
            
            $statement->execute();
        
            header("Location: /unitbase.php/Home/Editing");
        }
        
        else {
            return $this->view('Home', 'Error', null);
        }
    }
}