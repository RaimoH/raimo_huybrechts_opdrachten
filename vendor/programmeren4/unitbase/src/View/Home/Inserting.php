<main>
    <header>
        <h1>Programmeren 4 - Tussentijds Examen</h1>
    </header>
    <article>
        <nav>
            <a href="/unitbase.php/Home/Editing">Overzicht</a>
        </nav>
        <section>
            <form action="/unitbase.php/Home/Insert" method="post">
                <div>
                    <label for="UnitBaseName">Naam</label>
                    <input type="text" name="UnitBaseName" id="UnitBaseName" value="<?php echo isset($model) && !empty($model->getName()) ? $model->getName() : '' ?>"/>
                </div>
                <div>
                    <label for="UnitBaseDescription">Beschrijving</label>
                    <input type="text" name="UnitBaseDescription" id="UnitBaseDescription" value="<?php echo isset($model) && !empty($model->getDescription()) ? $model->getDescription() : '' ?>"/>
                </div>
                <div>
                    <label for="UnitBaseShippingCostMultiplier">Verzendkostenfactor</label>
                    <input type="text" name="UnitBaseShippingCostMultiplier" id="UnitShippingCostMultiplier"  value="<?php echo isset($model) && !empty($model->getShippingCostMultiplier()) ? $model->getShippingCostMultiplier() : '' ?>"/>
                </div>
                <div>
                    <label for="UnitBaseCode">Code</label>
                    <input type="text" name="UnitBaseCode" id="UnitBaseCode" value="<?php echo isset($model) && !empty($model->getCode()) ? $model->getCode() : '' ?>"/>
                </div>
                <br />
                <button type="submit">Insert</button>
            </form>
        </section>
    </article>
</main>

<?php $appStateView();?>
<pre><?php print_r($model->getModelState()->getBoard()); ?></pre>