<main>
    <header>
        <h1>Programmeren 4 - Tussentijds Examen</h1>
    </header>
    <article>
        <nav>
            <a href="/unitbase.php/Home/Editing">Overzicht</a>
        </nav>
        <section>
            <form action="/unitbase.php/Home/Update" method="post">
                <input type="text" name="UnitBaseId" value="<?php echo $model->getId();?>" readonly/>
                <div>
                    <label for="UnitBaseName">Naam</label>
                    <input type="text" name="UnitBaseName" id="UnitBaseName" value="<?php echo $model->getName();?>"/>
                </div>
                <div>
                    <label for="UnitBaseDescription">Beschrijving</label>
                    <input type="text" name="UnitBaseDescription" id="UnitBaseDescription" value="<?php echo $model->getDescription();?>"/>
                </div>
                <div>
                    <label for="UnitBaseShippingCostMultiplier">Verzendkostenfactor</label>
                    <input type="text" name="UnitBaseShippingCostMultiplier" id="UnitBaseShippingCostMultiplier" value="<?php echo $model->getShippingCostMultiplier();?>"/>
                </div>
                <div>
                    <label for="UnitBaseCode">Code</label>
                    <input type="text" name="UnitCode" id="UnitCode" value="<?php echo $model->getCode();?>"/>
                </div>
                <br />
                <button type="submit">Update</button>
            </form>
        </section>
    </article>
</main>

<?php $appStateView();?>