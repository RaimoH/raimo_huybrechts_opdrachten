<main>
    <header>
        <h1>Programmeren 4 - Tussentijds Examen</h1>
    </header>
    <article>
        <nav>
            <a href="/unitbase.php/Home/Inserting">Inserting</a>
        </nav>
        <section>
            <?php
                if (count($model->getList()) > 0) { ?>
            <table>
                <tr>
                    <th>Id</th>
                    <th>Naam</th>
                    <th>Verzendkostenfactor</th>
                    <th>Code</th>
                </tr>
                <?php foreach($model->getList() as $item) { ?>
                <tr>
                    <td>
                        <?php echo $item['Id'];?>
                    </td>
                    <td>
                        <?php echo $item['Name'];?>
                    </td>
                    <td>
                        <?php echo $item['ShippingCostMultiplier'];?>
                    </td>
                    <td>
                        <?php echo $item['Code'];?>
                    </td>
                    <td>
                        <td><a href="/unitbase.php/Home/Updating/<?php echo $value['Id']; ?>">Update</a> | <a href="/unitbase.php/Home/Delete/<?php echo $value['Id']; ?>">Delete</a></td>
                    </td>
                </tr>
                <?php
                }
                ?>
            </table>
            <?php
            } else { ?>
                <p>Geen unit bases gevonden!</p>
            <?php 
            } ?>
        </section>
    </article>
</main>

<?php $appStateView(); ?>