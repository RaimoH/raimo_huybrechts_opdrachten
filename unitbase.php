<?php
    include ( __DIR__ . '/vendor/autoload.php');
    
    $appState = new \ModernWays\Dialog\Model\NoticeBoard();
    $request = new \ModernWays\Mvc\Request('/Home/Editing');
    $route = new \ModernWays\Mvc\Route($appState, $request->uc());
    $routeConfig = new \ModernWays\Mvc\RouteConfig('\Programmeren4\UnitBase', $route, $appState);
    $view = $routeConfig->invokeActionMethod();
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Programmeren 4 - Tussentijds Examen</title>
        <link rel="stylesheet" href="css/style.css" type="text/css" />
    </head>
    <body>
        <?php $view();?>
    </body>
</html>